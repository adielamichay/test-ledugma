import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  deleteItem(key:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').remove(key);
    })
  }

  updateStock(key:string,stock:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').update(key,{'stock':stock});
    })
    
  }

  constructor(private db:AngularFireDatabase, 
              private authService:AuthService) { }
}
