import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;
  name:string;
  price:string;
  stock:boolean
  key:string;
  showTheButton = false;
  showDeleteField = false;

  showButton(){
    this.showTheButton = true;
  }
  hideButton(){
    this.showTheButton = false;
  }


  showEdit(){
    this.showDeleteField = true;
    this.showTheButton = false;
  }

  cancel(){
    this.showDeleteField = false;
  }
  
  toDelete(){
    this.itemsServics.deleteItem(this.key);
  }

  checkChange()
  {
   
    this.itemsServics.updateStock(this.key,this.stock);
  }

  constructor(private itemsServics:ItemsService) { }

  ngOnInit() {

    this.name = this.data.name;
    this.price=this.data.price;
    this.stock=this.data.stock;
    this.key = this.data.$key;
  }

}
