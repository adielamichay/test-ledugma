import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})

export class GreetingsComponent implements OnInit {
 email='';

  constructor(public authService:AuthService) { 
   
  }
  ngOnInit() {
     this.authService.user.subscribe(user=>{
    console.log(user.email);
    this.email=user.email;
  });
  }

}
