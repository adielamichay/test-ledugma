import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  logout(){
    return this.fireBaseAuth.auth.signOut();
  }

  login(email: string, password: string){
    return this.fireBaseAuth
                .auth.signInWithEmailAndPassword(email, password);
  }

   user:Observable<firebase.User>;
  constructor(private fireBaseAuth:AngularFireAuth) { 
    this.user = fireBaseAuth.authState;
  }
}
