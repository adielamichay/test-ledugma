import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  itemTextFromTodo='No items so far';
  items = [ ];

 

  constructor(private router:Router,
              public authService:AuthService,
              private db:AngularFireDatabase) { }

  ngOnInit() {
    console.log(this.items.length)
    this.authService.user.subscribe(user => {//לבדוק בדאטה בייס מה הנתיב כדי להגיע למקום הנכון
      this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(
        items =>{
          this.items = [];
          items.forEach(
            item => {
              let y =item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
              console.log(this.items);
 
            }
          )
        }
      ) 
    })
  }
}


